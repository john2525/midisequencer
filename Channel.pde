
class Channel {

  private PVector center;
  private int diameter;
  private int radius;
  private int myHue;
  private color myColor;
  private boolean active;
  private boolean isOn;
  private Note note;

  Channel(float x, float y, int dia, int hue) {
    center = new PVector(x, y);
    diameter = dia;
    radius = dia / 2;
    myHue = hue;
    active = false;
    isOn = false;
  }

  public void checkMouseClick(int y) {
    if (abs(y - center.y) < radius) {
      isOn = !isOn;
    }
  }

  public Note getNote() {
    return note;
  }

  public void setNote(int channel, MidiNoteNumbers mNum, int velocity) {
    note = new Note(channel, mNum.valueOf(), velocity);
  }

  public void turnOn() {
    isOn = true;
  }

  public void turnOff() {
    isOn = false;
  }

  public boolean isOn() {
    return isOn;
  }

  void display(int theHue) {
    color noteColor = color(theHue, SAT_MAX, BRIGHT_MAX);
    fill(noteColor);
    noStroke();
    ellipse(center.x, center.y, diameter, diameter);
  }

  void display() {

    color noteColor = color(myHue, SAT_MAX, BRIGHT_MAX);

    if (isOn) {
      // println("note isOn, hue = " + myHue);
      fill(noteColor);
      noStroke();
    } else {
      // println("note isOn, hue = " + myHue);
      noFill();
      strokeWeight(2);
      stroke(0);
    }

    ellipse(center.x, center.y, diameter, diameter);
  }

}
