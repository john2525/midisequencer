
public enum MidiNoteNumbers {
  C1(36), CSHARP1(37), D1(38), DSHARP1(39), E1(40),F1(41), FSHARP1(42), G1(43), GSHARP1(44), A1(45), ASHARP1(46), B1(47),
  C2(48), CSHARP2(49), D2(50), DSHARP2(51), E2(52),F2(53), FSHARP2(54), G2(55), GSHARP2(56), A2(57), ASHARP2(58), B2(59),
  C3(60), CSHARP3(61), D3(62), DSHARP3(63), E3(64),F3(65), FSHARP3(66), G3(67), GSHARP3(68), A3(69), ASHARP3(70), B3(71),
  C4(72), CSHARP4(73), D4(74), DSHARP4(75), E4(76),F4(77), FSHARP4(78), G4(79), GSHARP4(80), A4(81), ASHARP4(82), B4(82);

  int value;

  MidiNoteNumbers(int v) {
    value = v;
  }

  int valueOf() {
    return value;
  }

};
