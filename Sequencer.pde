

class Sequencer {

  // private final int MARGIN_TOP_BOTTOM = int(height * 0.1);
  private int MARGIN_TOP_BOTTOM;
  private final int MARGIN_SIDE = int(width * 0.075);

  private final int GAP_VERT = int(height * 0.01);
  private final int GAP_HOR = int(width * 0.035);

  private final int CURSOR_HUE = int(HUE_MAX * 0.67);
  private final int BEATS_PER_MIN = 100;

  private TimeDivision[] timeDivs;
  private Cursor cursor;
  private int cursorPosition;
  private int previousCursorPosition;
  private MidiBus myBus;

  Sequencer(int numChannels, int numTimeDivs) {
    timeDivs = new TimeDivision[numTimeDivs];
    cursor = new Cursor(CURSOR_HUE, numTimeDivs, BEATS_PER_MIN);

    //                 Parent      In                    Out
    myBus = new MidiBus(this, "PhysicalSequencerIn", "PhysicalSequencerOut");

    float channelDiameter = (width - (MARGIN_SIDE * 2) - ((NUM_TIME_DIVS - 1) * GAP_HOR)) / NUM_TIME_DIVS;
    float channelDiameterHalf = channelDiameter / 2.0;

    // float channelSpacing = (height - (MARGIN_TOP_BOTTOM * 2) - ((numNotesPerTimeDiv - 1) * GAP_VERT)) / numNotesPerTimeDiv;
    int MARGIN_TOP_BOTTOM = int((height - (numChannels * channelDiameter) - ((numChannels - 1) * GAP_VERT)) / 2.0);

    float channelSpacing = channelDiameter + GAP_VERT;

    float positionY = MARGIN_TOP_BOTTOM + channelDiameterHalf;

    for (int i = 0; i < timeDivs.length; i++) {

        // calculate TD position (the center of the top channel)
        float positionX = MARGIN_SIDE + channelDiameterHalf + (i * (GAP_HOR + channelDiameter));
        PVector position = new PVector(positionX, positionY);
        timeDivs[i] = new TimeDivision(position, channelSpacing, int(channelDiameter), numChannels, myBus);
    }

  }

  void checkMouseClick(int x, int y) {
    for (int i = 0; i < timeDivs.length; i++) {
      // for (int j = 0; j < timeDivs[i].getNumNotes(); j++) {
        // if (dist(x, y, timeDivs[i].)
        timeDivs[i].checkMouseClick(x, y);
    }
  }

  void run() {
    update();
    display();
  }

  void update() {
    int newPos = cursor.update();  // Returns -1 if no change this call

    if (newPos >= 0) {
      cursorPosition = newPos;

      // println("current cursor position = " + cursorPosition +
      //         ", previous cursor position = " + cursor.getPrevPosition(cursorPosition));

      // Generate a noteOff message for all channels at the previous cursor position that are active
      timeDivs[cursor.getPrevPosition(cursorPosition)].setChannelsIfActive(ChannelState.OFF);

      // Generate a noteOn message for all channels at this cursor position that are active
      timeDivs[cursorPosition].setChannelsIfActive(ChannelState.ON);
    }
  }

  void display() {
    for (int i = 0; i < timeDivs.length; i++) {
      if (i == cursorPosition) {
        timeDivs[i].display(CURSOR_HUE);
      } else {
        timeDivs[i].display();
      }
    }

    if (isDebugView) {
      // write bpm
      String bpmString = "BPM: " + str(cursor.getBeatsPerMin());
      textSize(12);
      // float numberStringWidth = textWidth(numberString);
      float bpmStringAscent = textAscent();
      text(bpmString, 10, 10 + bpmStringAscent);
    }
  }

  public void incTempo() {
    cursor.incBeatsPerMin();
  }

  public void decTempo() {
    cursor.decBeatsPerMin();
  }

  public void clear() {
    for (int i = 0; i < timeDivs.length; i++) {
      timeDivs[i].clear();
    }
  }

}
