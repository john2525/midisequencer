
class TimeDivision {

  private final int VELOCITY_DEFAULT = 64;

  private Channel[] channels;
  private PVector position;
  private int noteRadius;
  private MidiBus myBus;

  TimeDivision(PVector pos, float noteSpacing, int noteDia, int numChannels, MidiBus midiBus) {

    position = pos;
    noteRadius = noteDia / 2;

    myBus = midiBus;

    channels = new Channel[numChannels];

    float centerX = position.x;

    float hueInc = (HUE_MAX * 0.83) / channels.length;
    // println("hueInc = " + hueInc);

    for (int i = 0; i < channels.length; i++) {
      float centerY = position.y + (i * noteSpacing);
      int noteHue = int(HUE_MIN + (i * hueInc));
      channels[i] = new Channel(centerX, centerY, noteDia, noteHue);

    // println("noteHue[" + i + "] = " + noteHue);

    }

    initializeChannelData();

  }

  private void initializeChannelData() {

    //                   Channel Pitch                Vel
    channels[ 0].setNote( 1, MidiNoteNumbers.C3,      VELOCITY_DEFAULT);
    channels[ 1].setNote( 1, MidiNoteNumbers.B2,      VELOCITY_DEFAULT);
    channels[ 2].setNote( 1, MidiNoteNumbers.G2,      VELOCITY_DEFAULT);
    channels[ 3].setNote( 1, MidiNoteNumbers.F2,      VELOCITY_DEFAULT);
    channels[ 4].setNote( 1, MidiNoteNumbers.E2,      VELOCITY_DEFAULT);
    channels[ 5].setNote( 1, MidiNoteNumbers.C2,      VELOCITY_DEFAULT);
    channels[ 6].setNote( 0, MidiNoteNumbers.C3,      VELOCITY_DEFAULT);
    channels[ 7].setNote( 0, MidiNoteNumbers.B2,      VELOCITY_DEFAULT);
    channels[ 8].setNote( 0, MidiNoteNumbers.G2,      VELOCITY_DEFAULT);
    channels[ 9].setNote( 0, MidiNoteNumbers.F2,      VELOCITY_DEFAULT);
    channels[10].setNote( 0, MidiNoteNumbers.E2,      VELOCITY_DEFAULT);
    channels[11].setNote( 0, MidiNoteNumbers.C2,      VELOCITY_DEFAULT);
    channels[12].setNote(15, MidiNoteNumbers.ASHARP1, VELOCITY_DEFAULT);
    channels[13].setNote(15, MidiNoteNumbers.FSHARP1, VELOCITY_DEFAULT);
    channels[14].setNote(15, MidiNoteNumbers.D1,      VELOCITY_DEFAULT);
    channels[15].setNote(15, MidiNoteNumbers.C1,      VELOCITY_DEFAULT);

  }

  public void setChannelsIfActive(ChannelState newState) {
    for (int i = 0; i < channels.length; i++) {
      if (channels[i].isOn()) {
        // println("turning on channel " + i);
        if (newState == ChannelState.ON) {
          myBus.sendNoteOn(channels[i].getNote());
        } else if (newState == ChannelState.OFF) {
          myBus.sendNoteOff(channels[i].getNote());
        }
      }
    }
  }

  public void display() {
    for (int i = 0; i < channels.length; i++) {
      channels[i].display();
    }
  }

  public void display(int cursorHue) {
    for (int i = 0; i < channels.length; i++) {
      channels[i].display(cursorHue);
    }
  }

  public void clear() {
    for (int i = 0; i < channels.length; i++) {
      channels[i].turnOff();
    }
  }

  public void checkMouseClick(int x, int y) {
    if (abs(x - position.x) < noteRadius) {
      for (int i = 0; i < channels.length; i++) {
        channels[i].checkMouseClick(y);
      }
    }
  }

}
