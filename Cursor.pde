
class Cursor {

  private final long BPM_SCALER = 60000;  // (60 sec / 1 min) * (1000 millis / 1 sec) = 60000 millis / min

  private int myHue;
  private int numTimeDivs;
  private int beatsPerMinute;
  private long millisDelay;
  private long previousMillis;
  private int position;

  Cursor(int _hue, int numTD, int bpm) {
    myHue = _hue;
    numTimeDivs = numTD;
    beatsPerMinute = bpm;
    millisDelay = BPM_SCALER / bpm;  // Calculate the delay based on the beats per minute
    previousMillis = millis();
  }

  public int update() {
    int result = -1;  // Return value if the cursor didn't advance this call

    if (millis() > previousMillis + millisDelay) {
      previousMillis = millis();

      if (position < numTimeDivs - 1) {
        position++;
      } else {
        position = 0;
      }

      result = position;  // Return the new position we moved to this call
    }

    return result;
  }

  public int getPrevPosition(int currentPos) {
    if (currentPos > 0) {
      return currentPos - 1;
    } else {
      return numTimeDivs - 1;
    }
  }

  public void setBeatsPerMin(int bpm) {
    beatsPerMinute = bpm;
    calcMillisDelay();
  }

  public int getBeatsPerMin() {
    return beatsPerMinute;
  }

  public void incBeatsPerMin() {
    beatsPerMinute++;
    calcMillisDelay();
  }

  public void decBeatsPerMin() {
    beatsPerMinute--;
    calcMillisDelay();
  }

  private void calcMillisDelay() {
    millisDelay = BPM_SCALER / beatsPerMinute;
  }

}
