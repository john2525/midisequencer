// Ex of Sequencer Configuration File
// MARGIN_SIDE
// MARGIN_TOP_BOTTOM
// GAP_HOR
// GAP_VERT
// NUM_CHANNELS
// NUM_TIME_DIVS
// Channel 1 Hue
// Channel 2 Hue
// Channel 3 Hue
// Channel 4 Hue
// Channel 5 Hue
// Channel 6 Hue
// Channel 7 Hue
// Channel 8 Hue
// Channel 9 Hue
// Channel 10 Hue
// Channel 11 Hue
// Channel 12 Hue
// Channel 14 Hue
// Channel 15 Hue
// Channel 16 Hue
// CursorHue
//

import themidibus.*; //Import the library

final int HUE_MIN = 0;
final int HUE_MAX = 100;
final int SAT_MIN = 0;
final int SAT_MAX = 100;
final int BRIGHT_MAX = 100;

final color WHITE = color(HUE_MIN, SAT_MIN, BRIGHT_MAX);

final int NUM_CHANNELS = 16;
final int NUM_TIME_DIVS = 16;

boolean isDebugView = false;
boolean isSaveImages = false;

Sequencer sequencer;

void setup() {
  //size(1200, 600, P2D);
  size(1200, 600, P3D);
  smooth(4);
  //frameRate(30);
  colorMode(HSB, HUE_MAX, SAT_MAX, BRIGHT_MAX);

  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.

  sequencer = new Sequencer(NUM_CHANNELS, NUM_TIME_DIVS);
}

void draw() {
  background(90);

  if (isDebugView) {
    //line(0, height / 2, width, height / 2);
  }

  sequencer.run();

  if (isSaveImages) {
    saveFrame("frames/####.png");
  }

}

void mousePressed() {
  sequencer.checkMouseClick(mouseX, mouseY);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      sequencer.incTempo();
    }
    else if (keyCode == DOWN) {
      sequencer.decTempo();
    }
  } else {
    switch (key) {
    //case '1':
    //  break;

    //case '2':
    //  break;

    case 'c':
    case 'C':
      sequencer.clear();
      break;

    case 'd':
    case 'D':
      isDebugView = !isDebugView;
      break;

    case 's':
    case 'S':
      isSaveImages = !isSaveImages;
      break;

    //case 'q':
    //  exit();
    //  break;
    }
  }
}
